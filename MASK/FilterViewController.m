//
//  FilterViewController.m
//  MASK
//
//  Created by Muneeb Awan on 04/03/2017.
//  Copyright © 2017 Awan_Sahab. All rights reserved.
//

#import "FilterViewController.h"
#import "ImageProcessor.h"
#import "UIImage+OrientationFix.h"
#import <UIKit/UIKit.h>

@interface FilterViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (strong, nonatomic) UIImagePickerController * imagePickerController;
@property (strong, nonatomic) UIImage * filteredImage; // filtered image
@property (strong, nonatomic) UIImage * workingImage; // the original image
@end

@implementation FilterViewController

#pragma mark View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    sliderOnDisplay = true;
    menueOnDisplay = true;
    
    filters = [ImageProcessor allFilters];
    buttonArray = [NSMutableArray array];
    self.workingImage = [UIImage imageNamed:@"up"];
    [self updateFilterMenu];
    
    otherView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    otherView.translatesAutoresizingMaskIntoConstraints = false;
    filtSlider.translatesAutoresizingMaskIntoConstraints = false;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void) updateFilterMenu {
    [buttonArray removeAllObjects];
    [scroller setContentSize:CGSizeMake((80*filters.count), 120)];
    NSInteger i = -1;
    for (NSString *filtName in filters) {
        i++;
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        // ADD TAG
        // TIMER AND THEN GET BUTTON FROM TAG ID THEN DO THE CALCULATION
        
        [button setImage:[ImageProcessor processUsingPixels:self.workingImage : filtName : 50] forState:UIControlStateNormal];
        button.frame = CGRectMake((i * 90) + 10, 5, 80, 120); //x, y, width, height
        [scroller addSubview:button];
        [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [buttonArray addObject:button];
    }
}

- (void)buttonPressed:(UIButton *)sender {
    NSUInteger indexB = [buttonArray indexOfObject:sender];
    filterName = [filters objectAtIndex:indexB];
    self.filteredImage = [ImageProcessor processUsingPixels:self.workingImage : filterName : 50];
    imgView.image = self.filteredImage;
    filtSlider.value = 50;
}

#pragma mark Image Selector

// X------------------ IMAGE SELECTOR ------------------X
- (UIImagePickerController *)imagePickerController {
    if (!_imagePickerController) {
        _imagePickerController = [[UIImagePickerController alloc] init];
        _imagePickerController.allowsEditing = NO;
        _imagePickerController.delegate = self;
    }
    return _imagePickerController;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [[picker presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [[picker presentingViewController] dismissViewControllerAnimated:YES completion:nil];
    [self setupWithImage:info[UIImagePickerControllerOriginalImage]];
    [self updateFilterMenu];
}

- (void)upImage:(UIImage *)image {
    NSMutableArray *sharingItems = [NSMutableArray new];
    if (image) {
        [sharingItems addObject:image];
    }
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    [self presentViewController:activityController animated:YES completion:nil];
}

#pragma mark IBActions

// X------------------ BUTTONS PRESS ------------------X
- (IBAction)galBPress:(UIButton *)sender {
    self.imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:self.imagePickerController animated:YES completion:nil];
    if (sliderOnDisplay || menueOnDisplay) {
        [self hideSlider];
        [self hideFilterMenu];
    }
}

- (IBAction)camBPress:(UIButton *)sender {
    self.imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:self.imagePickerController animated:YES completion:nil];
    if (sliderOnDisplay || menueOnDisplay) {
        [self hideSlider];
        [self hideFilterMenu];
    }
}

- (IBAction)filterBPress:(UIButton *)sender {
    if (!menueOnDisplay) {
        [self hideSlider];
        [self showFilterMenu];
    } else {
        [self hideFilterMenu];
        [self hideSlider];
    }
}

- (IBAction)intensityBPress:(UIButton *)sender {
    if (!sliderOnDisplay) {
        [self hideFilterMenu];
        [self showSlider];
    } else {
        [self hideFilterMenu];
        [self hideSlider];
    }
}

- (IBAction)compareBPress:(UIButton *)sender {
    [self hideFilterMenu];
    [self hideSlider];
    if (sender.selected) {
        [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
            imgView.image = self.filteredImage;
        } completion:^(BOOL finished) {
        }];
        sender.selected = false;
    } else {
        [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
            imgView.image = self.workingImage;
        } completion:^(BOOL finished) {
        }];
        sender.selected = true;
    }
}

- (IBAction)uploadBPress:(UIBarButtonItem *)sender {
    [self upImage:self.workingImage];
}

- (IBAction)saveBPress:(UIButton *)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Do You Want To Save The Current Image?"
                                                    message:@""
                                                   delegate:self
                                          cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Yes",nil];
    [alert show];
}

- (void) alertView:(UIAlertView*)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        
    }
    else if (buttonIndex == 1) {
        if (!self.workingImage) {
            return;
        }
        UIImageWriteToSavedPhotosAlbum(imgView.image, nil, nil, nil);
    }
}

- (IBAction)sliderToggle:(UISlider *)sender {
    int power = (int)sender.value;
    self.filteredImage = [ImageProcessor processUsingPixels:self.workingImage : filterName : power];
    imgView.image = self.filteredImage;
}

#pragma mark Setup Image

// X------------------ SETUP IMAGE ON SCREEN ------------------X
- (void)setupWithImage:(UIImage*)image {
    UIImage * fixedImage = [image imageWithFixedOrientation];
    self.workingImage = fixedImage;
    imgView.image = fixedImage;
}

#pragma mark View Display

// X------------------ FILTER MENUE DISPLAY ------------------X
- (void) showFilterMenu {
    menueOnDisplay = true;
    [self.view addSubview:otherView];
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:otherView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:imgView attribute:NSLayoutAttributeBottom multiplier:1 constant: 0];
    
    NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem:otherView attribute:NSLayoutAttributeLeft relatedBy:(NSLayoutRelationEqual) toItem:imgView attribute:NSLayoutAttributeLeft multiplier:1 constant:0];
    
    NSLayoutConstraint *right = [NSLayoutConstraint constraintWithItem:otherView attribute:NSLayoutAttributeRight relatedBy:(NSLayoutRelationEqual) toItem:imgView attribute:NSLayoutAttributeRight multiplier:1 constant:0];
    
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:otherView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:128];
    
    [self.view addConstraints:@[height, bottom, left, right]];
    
    [otherView setAlpha:0];
    
    [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        [otherView setAlpha:1.0];
    } completion:^(BOOL finished) {
    }];
}

- (void) hideFilterMenu {
    menueOnDisplay = false;
    [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        [otherView setAlpha:0.0];
    } completion:^(BOOL finished) {
        [otherView removeFromSuperview];
    }];
}

// X------------------ SLIDER DISPLAY ------------------X
- (void) showSlider {
    sliderOnDisplay = true;
    [self.view addSubview:filtSlider];
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:filtSlider attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:imgView attribute:NSLayoutAttributeBottom multiplier:1 constant: 0];
    
    NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem:filtSlider attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:imgView attribute:NSLayoutAttributeLeft multiplier:1 constant:0];
    
    NSLayoutConstraint *right = [NSLayoutConstraint constraintWithItem:filtSlider attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:imgView attribute:NSLayoutAttributeRight multiplier:1 constant:0];
    
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:filtSlider attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:50];
    
    [self.view addConstraints:@[height, bottom, left, right]];
    
    [filtSlider setAlpha:0];
    
    [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        [filtSlider setAlpha:1.0];
    } completion:^(BOOL finished) {
    }];
}

- (void) hideSlider {
    sliderOnDisplay = false;
    [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        [filtSlider setAlpha:0.0];
    } completion:^(BOOL finished) {
        [filtSlider removeFromSuperview];
    }];
}

@end
