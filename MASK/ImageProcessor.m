//
//  ImageProcessor.m
//  MASK
//
//  Created by Muneeb Awan on 26/02/2017.
//  Copyright © 2017 Awan_Sahab. All rights reserved.
//

#import "ImageProcessor.h"

@interface ImageProcessor ()

@end

@implementation ImageProcessor

#define Mask8(x) ( (x) & 0xFF )
#define R(x) ( Mask8(x) )
#define G(x) ( Mask8(x >> 8 ) )
#define B(x) ( Mask8(x >> 16) )
#define A(x) ( Mask8(x >> 24) )
#define RGBAMake(r, g, b, a) ( Mask8(r) | Mask8(g) << 8 | Mask8(b) << 16 | Mask8(a) << 24 )

+ (NSArray*) allFilters {
    NSArray* nameArr = [NSArray arrayWithObjects:
                        @"Red Filter",
                        @"Green Filter",
                        @"Blue Filter",
                        @"Purple Filter",
                        @"Grey Filter",
                        nil];
    return nameArr;
}

// X------------------ FILTERS ------------------X
+ (void) redFilter:(UInt32)color : (UInt32 *) currentPixel : (int) power {
    UInt32 modR = R(color);
    modR = MAX(0,MIN(255, (modR+power)));
    *currentPixel = RGBAMake(modR, G(color), B(color), A(color));
}

+ (void) greenFilter:(UInt32)color : (UInt32 *) currentPixel : (int) power {
    UInt32 modG = G(color);
    modG = MAX(0,MIN(255, (modG+power)));
    *currentPixel = RGBAMake(R(color), modG, B(color), A(color));
}

+ (void) blueFilter:(UInt32)color : (UInt32 *) currentPixel : (int) power {
    UInt32 modB = B(color);
    modB = MAX(0,MIN(255, (modB+power)));
    *currentPixel = RGBAMake(R(color), G(color), modB, A(color));
}

+ (void) purpleFilter:(UInt32)color : (UInt32 *) currentPixel : (int) power {
    UInt32 modB = B(color);
    UInt32 modR = R(color);
    modR = MAX(0,MIN(255, (modR+power)));
    modB = MAX(0,MIN(255, (modB+power)));
    *currentPixel = RGBAMake(modR, G(color), modB, A(color));
}

+ (void) greyFilter:(UInt32)color : (UInt32 *) currentPixel : (int) power {
    UInt32 averageColor = (R(power+color) + G(power+color) + B(power+color))/3;
    *currentPixel = RGBAMake(averageColor, averageColor, averageColor, A(color));
}

// X------------------ IMAGE MODIFICATION ------------------X
+ (UIImage *)processUsingPixels:(UIImage*)inputImage : (NSString *) filterName  : (int) power {
    UInt32 * inputPixels;
    
    CGImageRef inputCGImage = [inputImage CGImage];
    NSUInteger inputWidth = CGImageGetWidth(inputCGImage);
    NSUInteger inputHeight = CGImageGetHeight(inputCGImage);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    NSUInteger bytesPerPixel = 4;
    NSUInteger bitsPerComponent = 8;
    
    NSUInteger inputBytesPerRow = bytesPerPixel * inputWidth;
    
    inputPixels = (UInt32 *)calloc(inputHeight * inputWidth, sizeof(UInt32));
    
    CGContextRef context = CGBitmapContextCreate(inputPixels, inputWidth, inputHeight,
                                                 bitsPerComponent, inputBytesPerRow, colorSpace,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    
    CGContextDrawImage(context, CGRectMake(0, 0, inputWidth, inputHeight), inputCGImage);
    
    for (NSUInteger y = 0; y < inputHeight; y++) {
        for (NSUInteger x = 0; x < inputWidth; x++) {
            UInt32 * currentPixel = inputPixels + (y * inputWidth) + x;
            UInt32 color = *currentPixel;
            
            if ([filterName  isEqual: @"Red Filter"]) {
                [self redFilter :color :currentPixel: power];
            } else if ([filterName  isEqual: @"Green Filter"]) {
                [self greenFilter :color :currentPixel: power];
            } else if ([filterName  isEqual: @"Blue Filter"]) {
                [self blueFilter :color :currentPixel: power];
            } else if ([filterName  isEqual: @"Purple Filter"]) {
                [self purpleFilter :color :currentPixel: power];
            } else if ([filterName  isEqual: @"Grey Filter"]) {
                [self greyFilter :color :currentPixel: 0];
            }
        }
    }
    
    CGImageRef newCGImage = CGBitmapContextCreateImage(context);
    UIImage * processedImage = [UIImage imageWithCGImage:newCGImage];
    
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    free(inputPixels);
    return processedImage;
}

@end
