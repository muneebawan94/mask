//
//  FilterViewController.h
//  MASK
//
//  Created by Muneeb Awan on 04/03/2017.
//  Copyright © 2017 Awan_Sahab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterViewController : UIViewController {
	IBOutlet UIImageView *imgView;
	IBOutlet UIButton *galB;
	IBOutlet UIButton *camB;
	IBOutlet UIButton *intB;
	IBOutlet UIButton *cmpB;
	IBOutlet UIButton *saveB;
	IBOutlet UISlider *filtSlider;
	
	NSString * filterName;
    IBOutlet UIScrollView *scroller;
    
    BOOL sliderOnDisplay;
    BOOL menueOnDisplay;
    
    IBOutlet UIView *otherView;
    NSArray * filters;
    
    NSMutableArray * buttonArray;
}

@end
