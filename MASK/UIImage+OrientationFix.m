//
//  UIImage+OrientationFix.h
//  MASK
//
//  Created by Muneeb Awan on 26/02/2017.
//  Copyright © 2017 Awan_Sahab. All rights reserved.
//

#import "UIImage+OrientationFix.h"

@implementation UIImage (OrientationFix)

- (UIImage *)imageWithFixedOrientation {
  if (self.imageOrientation == UIImageOrientationUp) return self;
  
  UIGraphicsBeginImageContextWithOptions(self.size, NO, self.scale);
  [self drawInRect:(CGRect){0, 0, self.size}];
  UIImage *normalizedImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return normalizedImage;
}

@end
