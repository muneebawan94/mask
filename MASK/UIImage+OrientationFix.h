//
//  UIImage+OrientationFix.h
//  MASK
//
//  Created by Muneeb Awan on 26/02/2017.
//  Copyright © 2017 Awan_Sahab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (OrientationFix)

- (UIImage *)imageWithFixedOrientation;

@end
