//
//  ImageProcessor.h
//  MASK
//
//  Created by Muneeb Awan on 26/02/2017.
//  Copyright © 2017 Awan_Sahab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ImageProcessor : NSObject

+ (NSArray*) allFilters;

+ (void) redFilter:(UInt32)color : (UInt32 *) currentPixel : (int) power;
+ (void) greenFilter:(UInt32)color : (UInt32 *) currentPixel : (int) power;
+ (void) blueFilter:(UInt32)color : (UInt32 *) currentPixel : (int) power;
+ (void) purpleFilter:(UInt32)color : (UInt32 *) currentPixel : (int) power;
+ (void) greyFilter:(UInt32)color : (UInt32 *) currentPixel : (int) power;

+ (UIImage *)processUsingPixels:(UIImage*)inputImage : (NSString *) filterName  : (int) power;
@end
